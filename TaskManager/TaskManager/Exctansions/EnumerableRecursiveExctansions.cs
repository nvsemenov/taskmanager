﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaskManager.Exctansions
{
    public static class EnumerableRecursiveExctansions
    {
        public static TV GetRoot<TK, TV>(this IEnumerable<TV> items, Func<TV, TK> selfIdSelector, Func<TV, TV> parentSelector)
        {
            var roots = items.ToList();
            var children = new List<TV>();
            foreach (var item in items)
            {
                if (parentSelector(item) != null && roots.Exists(x => selfIdSelector(x).Equals(selfIdSelector(parentSelector(item)))))
                    children.Add(item);
            }

            roots = roots.Except(children).ToList();
            if (roots.Count != 1)
            {
                throw new ArgumentException("items must have only one root");
            }

            return roots[0];
        }

        public static IEnumerable<TV> RecursiveTree<TK, TV>(this IEnumerable<TV> items, TK idRoot, Func<TV, TK> selfIdSelector, Func<TV, TK> parentIdSelector, Func<TV, IEnumerable<TV>> childrenSelector)
        {
            return Traverse(items, idRoot, selfIdSelector, parentIdSelector, childrenSelector).Distinct();
        }

        private static IEnumerable<TV> Traverse<TK, TV>(IEnumerable<TV> items, TK idRoot, Func<TV, TK> selfIdSelector, Func<TV, TK> parentIdSelector,
            Func<TV, IEnumerable<TV>> childrenSelector)
        {
            var tree = new List<TK> { idRoot };
            var stack = new Stack<TV>(items);
            while (stack.Any())
            {
                var next = stack.Pop();
                if (tree.Contains(selfIdSelector(next)))
                    yield return next;
                else if (tree.Contains(parentIdSelector(next)))
                {
                    tree.Add(selfIdSelector(next));
                    yield return next;
                }
                if (childrenSelector(next) != null)
                    foreach (var child in childrenSelector(next))
                        stack.Push(child);
            }
        }
    }
}