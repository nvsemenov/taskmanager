﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TaskManager.Enums;
using TaskManager.Exctansions;
using TaskManager.Models.Entities;

namespace TaskManager.Models.DTO
{
    public class JobDTO
    {
        #region Constructors

        public JobDTO(IEnumerable<Job> jobs)
        {
            if (jobs == null)
                throw new ArgumentNullException("jobs");

            var root = jobs.GetRoot(x => x.Id, x => x.Parent);
            Id = root.Id;
            ParentId = root.ParentId;
            Name = root.Name;
            Executant = root.Executant;
            DateCreated = root.DateCreated;
            Status = root.Status;
            PlannedTime = root.PlannedTime;
            ActualTime = root.ActualTime;
            DateFinished = root.DateFinished;
            AllPlannedTime = jobs.Sum(x => x.PlannedTime);
            AllActualTime = jobs.Sum(x => x.ActualTime);
            CanFinished = jobs.All(x => x.Status != JobStatus.Created);
        }

        #endregion

        #region Properties

        public int Id { get; private set; }
        public int? ParentId { get; private set; }
        public string Name { get; private set; }
        public string Executant { get; private set; }

        [JsonConverter(typeof(DateTimeConverterBase))]
        public DateTime DateCreated { get; private set; }

        public JobStatus Status { get; private set; }
        public int PlannedTime { get; private set; }
        public int? ActualTime { get; private set; }

        [JsonConverter(typeof(DateTimeConverterBase))]
        public DateTime? DateFinished { get; private set; }

        public int AllPlannedTime { get; private set; }
        public int? AllActualTime { get; private set; }
        public bool CanFinished { get; private set; }

        #endregion
    }
}