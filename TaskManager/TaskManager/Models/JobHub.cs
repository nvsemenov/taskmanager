﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace TaskManager.Models
{
    public class JobHub : Hub
    {
        [HubMethodName("Notify")]
        public static void Notify()
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<JobHub>();

            // the update client method will update the connected client about any recent changes in the server data
            context.Clients.All.updateJobs();
        }
    }
}