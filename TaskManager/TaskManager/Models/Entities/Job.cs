﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TaskManager.Enums;

namespace TaskManager.Models.Entities
{
    public class Job
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Executant { get; set; }

        [JsonConverter(typeof(DateTimeConverterBase))]
        public DateTime DateCreated { get; set; }

        public JobStatus Status { get; set; }
        public int PlannedTime { get; set; }
        public int? ActualTime { get; set; }

        [JsonConverter(typeof(DateTimeConverterBase))]
        public DateTime? DateFinished { get; set; }

        public Job Parent { get; set; }
        public IList<Job> Children { get; set; }
    }
}