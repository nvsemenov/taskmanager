﻿using System.Data.Entity;
using TaskManager.Models.Entities;

namespace TaskManager.Models
{
    public class JobContext : DbContext
    {
        public JobContext()
            : base("DefaultConnection") { }

        public DbSet<Job> Jobs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Job>()
                .HasOptional(h => h.Parent)
                .WithMany(x => x.Children)
                .HasForeignKey(x => x.ParentId)
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}