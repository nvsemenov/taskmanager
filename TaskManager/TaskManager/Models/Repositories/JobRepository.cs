﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TaskManager.Enums;
using TaskManager.Exctansions;
using TaskManager.Interfaces;
using TaskManager.Models.Entities;

namespace TaskManager.Models.Repositories
{
    public class JobRepository : IJobRepository
    {
        public IEnumerable<Job> GetAll()
        {
            using (var context = new JobContext())
            {
                return context.Jobs.ToList();
            }
        }

        public Job Get(int id)
        {
            using (var context = new JobContext())
            {
                return context.Jobs.SingleOrDefault(x => x.Id == id);
            }
        }

        public void Add(Job job)
        {
            using (var context = new JobContext())
            {
                context.Jobs.Add(job);
                context.SaveChanges();
            }
        }

        public void Update(Job job)
        {
            using (var context = new JobContext())
            {
                if (job.Status == JobStatus.Issued)
                {
                    var recursiveList = context.Jobs.RecursiveTree(job.Id, x => x.Id, x => x.ParentId, x => x.Children).ToList();
                    foreach (var item in recursiveList)
                    {
                        item.DateFinished = DateTime.Now;
                        item.Status = JobStatus.Issued;
                        context.Entry(item).State = EntityState.Modified;
                    }
                }
                else
                    context.Entry(job).State = EntityState.Modified;

                context.SaveChanges();
            }
        }

        public IEnumerable<int> Delete(int id)
        {
            using (var context = new JobContext())
            {
                var recursiveList = context.Jobs.RecursiveTree(id, x => x.Id, x => x.ParentId, x => x.Children).ToList();
                foreach (var item in recursiveList)
                {
                    context.Jobs.Remove(item);
                }

                context.SaveChanges();
                return recursiveList.Select(x => x.Id);
            }
        }
    }
}