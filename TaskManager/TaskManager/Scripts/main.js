﻿$(function () {
    // Create a proxy to signalr hub on web server. It reference the hub.
    var notificationFromHub = $.connection.jobHub;

    // Connect to signalr hub
    $.connection.hub.start().done(function () { });

    // Notify to client with the recent updates
    notificationFromHub.client.updateJobs = function () {
        FetchData();
    };
});

function FetchData() {
    location.reload();
}

$(".list-group-item").click(function () {
    $(".list-group-item").filter(".active").removeClass("active");
    $(this).addClass("active");
    var id = $(this)[0].id.slice(5, -4);
    $.getJSON("/Job/Get/" + id, null, updateDetail);
});


(function () {
    "use strict";
    window.addEventListener("load",
        function () {
            var forms = document.getElementsByClassName("needs-validation");
            Array.prototype.filter.call(forms,
                function (form) {
                    form.addEventListener("submit",
                        function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add("was-validated");
                        },
                        false);
                });
        },
        false);
})();

function deleteJob() {
    $.ajax({
        type: "DELETE",
        url: "/Job/Delete/" + $("#inputId").val()
    });
    return false;
}

function updateDetail(data) {
    console.info(data);
    $("#inputId").val(data.Id);
    $("#inputParentId").val(data.ParentId);
    $("#inputName").val(data.Name);
    $("#inputStatus").val(data.Status);
    $("#inputExecutant").val(data.Executant);
    $("#inputDateCreated").val(jsonDateParser(data.DateCreated));
    $("#inputDateFinished").val(jsonDateParser(data.DateFinished));
    $("#inputTimePlan").val(data.PlannedTime);
    $("#inputTimeFact").val(data.ActualTime);
    $("#inputAllTimePlan").val(data.AllPlannedTime);
    $("#inputAllTimeFact").val(data.AllActualTime);
    setControlState(data.Status, data.CanFinished);
}

function createChild() {
    $("#inputParentId").val($("#inputId").val());
    $("#inputId").val(0);
    $("#inputName").val("");
    $("#inputStatus").val(0);
    $("#inputExecutant").val("");
    $("#inputDateCreated").val(moment(new Date()).format("DD.MM.YYYY HH:mm:ss"));
    $("#inputDateFinished").val(null);
    $("#inputTimePlan").val(0);
    $("#inputTimeFact").val(null);
    $("#inputAllTimePlan").val(null);
    $("#inputAllTimeFact").val(null);
    setControlState("", false);
}

function createNew() {
    $("#inputParentId").val(null);
    $("#inputId").val(0);
    $("#inputName").val("");
    $("#inputStatus").val(0);
    $("#inputExecutant").val("");
    $("#inputDateCreated").val(moment(new Date()).format("DD.MM.YYYY HH:mm:ss"));
    $("#inputDateFinished").val(null);
    $("#inputTimePlan").val(0);
    $("#inputTimeFact").val(null);
    $("#inputAllTimePlan").val(null);
    $("#inputAllTimeFact").val(null);
    setControlState("", false);
    $(".list-group-item").filter(".active").removeClass("active");
    $("#buttonAddChild").attr("disabled", "");
}

function setControlState(status, canFinished) {
    $("#buttonSave").removeAttr("hidden");
    $("#buttonSave").removeAttr("disabled");
    $("#buttonAdd").attr("hidden", "");
    $("#buttonAddChild").removeAttr("disabled");
    $("#buttonDelete").removeAttr("disabled");
    switch (status) {
        case 0:
            $("#statusOptionCreated").removeAttr("hidden");
            $("#statusOptionPaused").removeAttr("hidden");
            $("#statusOptionWorking").removeAttr("hidden");
            $("#statusOptionIssued").attr("hidden", "");
            break;
        case 5:
        case 10:
            $("#statusOptionCreated").attr("hidden", "");
            $("#statusOptionPaused").removeAttr("hidden");
            $("#statusOptionWorking").removeAttr("hidden");
            if (canFinished === true)
                $("#statusOptionIssued").removeAttr("hidden");
            else
                $("#statusOptionIssued").attr("hidden", "");
            break;
        case 20:
            $("#buttonSave").attr("disabled", "");
            $("#buttonAddChild").attr("disabled", "");
            $("#statusOptionCreated").attr("hidden", "");
            $("#statusOptionPaused").attr("hidden", "");
            $("#statusOptionWorking").attr("hidden", "");
            $("#statusOptionIssued").removeAttr("hidden");
            break;
        default:
            $("#buttonSave").attr("hidden", "");
            $("#buttonAdd").removeAttr("hidden");
            $("#buttonAddChild").attr("disabled", "");
            $("#buttonDelete").attr("disabled", "");
            $("#statusOptionCreated").removeAttr("hidden");
            $("#statusOptionPaused").attr("hidden", "");
            $("#statusOptionWorking").attr("hidden", "");
            $("#statusOptionIssued").attr("hidden", "");
            break;
    }
}

function jsonDateParser(jsonDate) {
    if (jsonDate !== null && typeof jsonDate !== "undefined") {
        var date = new Date(parseInt(jsonDate.substr(6)));
        return moment(date).format("DD.MM.YYYY HH:mm:ss");
    }
    console.info("jsonDateParser argument is null or undefined");
    return null;
}