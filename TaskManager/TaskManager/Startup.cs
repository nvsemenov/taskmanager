﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TaskManager.Startup))]
namespace TaskManager
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}