﻿using System.Web.Http;
using System.Web.Mvc;
using TaskManager.Models;
using TaskManager.Models.Entities;
using TaskManager.Models.Repositories;
using TaskManager.Services;

namespace TaskManager.Controllers
{
    public class JobController : Controller
    {
        #region Fields

        private readonly JobCacheService _cache;

        #endregion

        #region Constructors

        public JobController()
        {
            _cache = new JobCacheService(new JobRepository());
        }

        #endregion

        #region Methods

        public ActionResult Main()
        {
            ViewBag.Data = _cache.GetAll();
            return View();
        }

        public ActionResult Get([FromBody]int id)
        {
            return Json(_cache.Get(id), JsonRequestBehavior.AllowGet);
        }

        public void Post([FromBody] Job value)
        {
            if (value.Id > 0)
                _cache.Update(value);
            else
                _cache.Add(value);
            JobHub.Notify();
        }

        public void Delete(int id)
        {
            _cache.Delete(id);
            JobHub.Notify();
        }

        #endregion
    }
}