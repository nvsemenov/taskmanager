﻿using System.Collections.Generic;
using TaskManager.Models.Entities;

namespace TaskManager.Interfaces
{
    public interface IJobRepository
    {
        /// <summary>
        /// Get All Jobs
        /// </summary>
        /// <returns>Enumerable Job</returns>
        IEnumerable<Job> GetAll();

        /// <summary>
        /// Get Job by Id
        /// </summary>
        /// <param name="id">Job's Id</param>
        /// <returns>Job without children and parent</returns>
        Job Get(int id);

        /// <summary>
        /// Add new Job
        /// </summary>
        /// <param name="job">Job</param>
        void Add(Job job);

        /// <summary>
        /// Update Job. If new state is Issued, all children's Job status will change too. 
        /// </summary>
        /// <param name="job">Job</param>
        void Update(Job job);

        /// <summary>
        /// Delete Job with all childrens's Job.
        /// </summary>
        /// <param name="id">Job's Id</param>
        /// <returns>Enumerable Id of deleted Jobs</returns>
        IEnumerable<int> Delete(int id);
    }
}
