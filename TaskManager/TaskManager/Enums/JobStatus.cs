﻿namespace TaskManager.Enums
{
    /// <summary>
    ///     Статус задачи
    /// </summary>
    public enum JobStatus : byte
    {
        /// <summary>
        ///     Создана
        /// </summary>
        Created = 0,

        /// <summary>
        ///     Приостановлена
        /// </summary>
        Paused = 5,

        /// <summary>
        ///     В работе
        /// </summary>
        Working = 10,

        /// <summary>
        ///     Завершена
        /// </summary>
        Issued = 20
    }
}