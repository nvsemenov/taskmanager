﻿using System.Collections.Generic;
using System.Linq;
using TaskManager.Exctansions;
using TaskManager.Interfaces;
using TaskManager.Models.DTO;
using TaskManager.Models.Entities;

namespace TaskManager.Services
{
    public class JobCacheService
    {
        #region Fields

        private readonly IJobRepository _repository;
        private static IDictionary<int, Job> _cache;

        #endregion

        #region Constructors

        public JobCacheService(IJobRepository dataManager)
        {
            _repository = dataManager;

            if (_cache == null)
                _cache = _repository.GetAll().ToDictionary(x => x.Id);
        }

        #endregion

        #region Methods

        public IEnumerable<Job> GetAll()
        {
            return _cache.Values;
        }

        public JobDTO Get(int id)
        {
            var tree = _cache.Values.RecursiveTree(id, x => x.Id, x => x.ParentId, x => x.Children);
            return new JobDTO(tree);
        }

        public void Add(Job job)
        {
            _repository.Add(job);
            _cache.Add(job.Id, job);
        }

        public void Update(Job job)
        {
            _repository.Update(job);
            _cache[job.Id] = job;
        }

        public void Delete(int id)
        {
            var deletedJobs = _repository.Delete(id);
            foreach (var deletedJob in deletedJobs)
                _cache.Remove(deletedJob);
        }

        #endregion
    }
}